	--进销存管理系统设计文案
--超市管理系统数据库E-R模型


create database Supermarket_Management_System
go
use Supermarket_Management_System
go
--进货有进货信息，包含商品号、进货号、供应商号、货价、数量
create table Purchase_information
(
	CommodityNumber int primary key identity(1,1),--商品号
	PurchaseNo varchar(80) not null,   --进货号
	SupplierName nvarchar(80),        --供应商号
	PriceofGoods money,              --货价
	Number varchar(20)              --数量

)
go
--销售有销售信息，包含销售号、商品号、供应商号、单价、数量
create table Sales_Information
(
	Sales_number int primary key identity(1,1),--销售号
	Purchase_no varchar(80) not null,--商品号
	Supplier_name nvarchar(80),--供应商号
	UnitPrice money,--单价
	Numbers varchar(20)--数量

)
go
--进货方面需要与销售方面相互取得沟通，进货方了解商品差价，
--进货方面需了解员工信息，方便退货处理；
--销售需要掌握供应商信息、库存信息以及处理退货售后

--供应商信息包含供应商名、供应商号、联系人、电话、地址
create table Supplier_information
(
	SupplierName int primary key  ,--供应商名
	SupplierNum varchar(80) not null,--供应商号
    Contacts varchar(20) not null,	--联系人
	Telephone nvarchar(20) not null,--电话
	Address nvarchar(80) not null --地址
)
go
--库存信息包含商品名、商品号、进货价、单价、供应商名、供应商号
create table Inventory_information
(
	TradeName int primary key,	--商品名
	Purchase_num varchar(80) not null,--商品号
	BuyingPrice money,--进货价
	Unit_price money,--单价
	Supplier_name varchar(10) not null, --供应商名
	Supplier_Num nvarchar(80) not null --供应商号

)
go
--员工信息包含姓名、电话、密码、编号、地址
create table Employee_information
(
	Employee_name int primary key  ,
	Employee_telephone varchar(20) not null,
	Employee_password varchar(20) not null,
	Employee_number varchar(20) not null,
	Employee_address nvarchar(80) not null

)
go
--进货和销售也许同时负责退货方面的管理
--退货信息包含销售号、商品号、退货号、单价、供应商号、退货原因
create table Return_information
(
	SalesNumber int primary key identity,--销售号
	Commodity_number varchar(80) not null,--商品号
	Return_number varchar(80) not null,--退货号
	Unit_Price money,--单价
	Supplier_Name nvarchar(80) not null, --供应商号
	ReasonforReturn nvarchar(80) not null --退货原因
)
go

 
select * from Purchase_Information
select * from Sales_Information
select * from Supplier_information
select * from Inventory_information
select * from Employee_information
select * from Return_information