create database purchase_sell_stock  --进销存数据库
go
use purchase_sell_stock
go

create table Users(--用户表
	Users_Id int not null primary key identity(1,1),  --用户id
	Users_Name nvarchar(20) not null,  --用户名
	Users_Pwd varchar(20) not null,  --用户密码
	Users_Real nvarchar(20) not null,  --真实姓名
	Users_Code varchar(30) not null,  --身份证号


)
go

create table Supplier(--供应商表
	Supplier_Id int not null primary key identity(1,1),  --供应商id
	Supplier_Name nvarchar(20) not null,  --供应商名称
	Supplier_Phone varchar(20) not null,  --供应商联系方式
	
)
go

create table Commodity(--商品表
	Sommodity_Id int not null primary key identity(1,1),  --商品id
	Sommodity_Type nvarchar(10) not null,  --商品类型
	Sommodity_Name nvarchar(20) not null,  --商品名称
	Sommodity_Price money not null,  --商品价格
	Supplier_Id int not null,  --供应商id
)
go

create table Stock(--进货表
	Stock_Id int not null primary key identity(1,1),  --进货id
	Sommodity_Type nvarchar(10) not null,  --商品类型
	Sommodity_Name nvarchar(20) not null,  --商品名称
	Stock_Spe int not null,  --进货规格
	Sommodity_Price money not null,  --商品价格
	Supplier_Name nvarchar(20) not null,  --供应商名称
	Supplier_Phone varchar(20) not null,  --供应商联系方式
	Stock_InTime Time not null  --进货时间


)
go

create table Bank(--仓库表
	Bank_Id int not null primary key identity(1,1),  --仓库id
	Bank_Zone nvarchar(10) not null,  --仓库分区
	Bank_Site nvarchar(30) not null,  --仓库地址
	Bank_Principal nvarchar(10) not null,  --仓库负责人
	Principal_Phone varchar(20) not null,  --负责人联系方式


)
go

create table Sell(--销售表
	Sell_Id int not null primary key identity(1,1),  --销售id
	Users_Name nvarchar(20) not null,  --用户名(收货人)
	Sommodity_Name nvarchar(20) not null,  --商品名称（出售商品）
	Sell_Price money not null,  --交易金额
	Sell_Time Time not null,  --交易时间
	Sell_Site nvarchar(30) not null  --收货方地址


)
go

create table OutBank(--出库表
	OutBank_Id int not null primary key identity(1,1),  --出库id
	OutBank_Type varchar(1) not null,  --出库类型
	--出库类型：1：销售出库，2：租借出库，3：转仓出库，4：赠送出库
	Bank_Zone nvarchar(10) not null,  --仓库分区
	Users_Name nvarchar(20) not null,  --用户名(收货人)
	Sell_Site nvarchar(30) not null,  --收货方地址
	

)
go

create table Inventory(--库存表
	Inventory_Id int not null primary key identity(1,1),  --库存id
	Sommodity_Id int not null,  --商品id
	Sommodity_Type nvarchar(10) not null,  --商品类型
	Sommodity_Name nvarchar(20) not null,  --商品名称
	Bank_Zone nvarchar(10) not null,  --仓库分区
	Bank_Principal nvarchar(10) not null,  --仓库负责人

)
go
