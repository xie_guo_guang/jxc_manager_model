create database Books
go

use Books
go



--------
--客户表
--------
create table Customer(
CustomerId int primary key identity(1,1), --客户id
CustomerName nvarchar(50) not null, --客户名称
CustomerSite nvarchar(50) not null, --客户地址
CustomerPhone varchar(30) not null --客户联系电话
)


--------
--供应商
--------
create table Supplier(
SupplierId int primary key identity(1,1), --供应商id
SuppliernName nvarchar(50) not null, --供应商名称
SupplierSite nvarchar(50) not null, --供应商地址
SupplierPhone varchar(30) not null, --供应商联系电话
SupplyOfGoodsId int not null, --供货商品id
SupplyOfGoodName nvarchar(50) not null --供货商品名称
)


--------
--销售单
--------
create table SalesList(
SalesId int primary key identity(1,1), --订单id
CustomerId int not null, --客户id
ProductId int not null, --产品id
ProductName nvarchar(50) not null, --产品名称
SalesSpecification varchar(20) not null, --产品规格
SalesQuantity varchar(20) not null, --产品数量
SalesPrice varchar(20) not null, --产品单价
SalesRental varchar(20) not null, --订单总额
SalesTime  smalldatetime default getdate() --交易时间
)


------------
--产品信息表
------------
create table ProductInformation(
ProductId int primary key identity(1,1), --产品id
ProductName nvarchar(50) not null, --产品名称
ProductSpecification varchar(20) not null, --产品规格
ProductInventory varchar(20) not null, --产品库存
ProductPrice varchar(20) not null --产品价格
)


--------
--进货单
--------
create table PurchaseList(
PurchaseId int primary key identity(1,1), --订单id
SupplierId int not null, --供应商id
SupplyOfGoodsId int not null, --产品id
SupplyOfGoodName nvarchar(50) not null, --产品名称
PurchaseSpecification varchar(20) not null, --产品规格
PurchaseQuantity varchar(20) not null, --产品数量
PurchasePrice varchar(20) not null, --产品单价
PurchaseRental varchar(20) not null, --订单总额
PurchaseTime smalldatetime default getdate() --交易时间
)


----------
--利润清单
----------
create table Incomestatement(
 Months varchar(20) not null, --月份
 OrderQuantity varchar(20) not null, --订单量
 MonthlyExpense varchar(20) not null, --月支出
 MonthlyIncome varchar(20) not null, --月收入
RetainedProfits  varchar(20) not null --净利润
)
