-- 创建库
--------------------
create database Invoicing
go

use Invoicing;
go

--创建表
-----------------
--职工信息表
create table Staff(
Staffed char(6) not null ,---编号
StaffName nvarchar(8) not null,---姓名
StaffSex nchar(2) not null, ---性别
staffpalce nvarchar(4)not null,---籍贯

)

--用户信息表
create table UserInfo(
UserId char(6) not null,--账号
Pass_word char(6)not null,--密码

)

--商品信息表

create table Commodity (
ProductID char(6) not null,--商品编号
SupplierID char(6) not null,--供货编号
ProductName nvarchar(8) not null,--商品名称
CommodityCost money not null,--商品单价
StockNumber int not null,--库存数量


)



--供货商信息表

create table  information(
ProductID char(6) not null,--商品编号
TradeName char(6) not null,--商品名称
Contacts   nvarchar(8) not null,--联系人
contactnumber varchar(11) not null,--联系电话
Place nvarchar(4)not null,--地址

)


-- 库存清单表
create table Inventory(
ProductID char(6) not null,--商品编号
StockNumber int not null,--库存数量
CommodityCost money not null,--商品单价
Contacts   nvarchar(8) not null,--联系人
contactnumber varchar(11) not null,--联系电话
Place nvarchar(4)not null,--地址


)
--销售信息表
create table SalesInfo(
SaleNo char(6) not null,--销售单号
MerchId char(6) not null ,--商品编号
SalesNum int not null,--销售数量
SaleDate datetime not null,--进货日期
Total Money not null,--金额
Other ntext null,--备注


)

--退货信息表
create table Returninformation(
number int not null ,--退货编号
ProductID char(6) not null,--供应商编号
CommodityCost money not null,--商品单价
StaffName nvarchar(8) not null,---售后人员姓名
contactnumber varchar(11) not null,--客户联系电话

)
--